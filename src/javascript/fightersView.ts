import { FighterDetails } from './FighterDetails';
import { Fighter } from './Fighter';
import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  handleClick: (ev: Event, f: Fighter) => void;

  constructor(fighters: Fighter[]) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }
 


  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event: Event, fighter: Fighter) {
    const { _id: id } = fighter;
    let fighterDetails:FighterDetails = await fighterService.getFighterDetails(id);

    console.log(fighterDetails);
    document.getElementById("heroName").innerText = fighterDetails.name;
    const saveButton = document.getElementById("saveButton");
    const modal = document.getElementById("myModal");
    const health = document.getElementById("health") as HTMLInputElement;
    const attack = document.getElementById("attack") as HTMLInputElement;
    const defense = document.getElementById("defense") as HTMLInputElement;
    health.value = fighterDetails.health.toString();
    attack.value = fighterDetails.attack.toString();
    defense.value = fighterDetails.defense.toString();
    modal.style.display = "block";

    saveButton.onclick = () =>{
      fighterDetails.health = parseInt(health.value);
      fighterDetails.attack = parseInt(attack.value);
      fighterDetails.defense = parseInt(defense.value);
      fighterService.setNewDetails(fighterDetails);
      modal.style.display = "none";
    }
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }

  
}

export default FightersView;