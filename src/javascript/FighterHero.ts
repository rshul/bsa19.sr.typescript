import { FighterDetails } from './FighterDetails';
export class FighterHero extends FighterDetails{
    constructor(details:FighterDetails){
        super();
        this.name = details.name;
        this._id = details._id;
        this.attack = details.attack;
        this.defense = details.defense;
        this.health = details.health;
        this.source = details.source;
        
    }
    getHitPower():number{
        let criticalHitChance = Math.floor(Math.random()*3)+1;
        return criticalHitChance * this.attack;
    }

    getBlockPower(): number{
        let dodgeChance = Math.floor(Math.random()*3)+1;
        return dodgeChance * this.defense;
    }
}