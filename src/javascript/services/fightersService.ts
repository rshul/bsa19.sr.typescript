import { FighterDetails } from './../FighterDetails';
import { Fighter } from './../Fighter';
import { callApi } from '../helpers/apiHelper';

class FighterService {

  fightersDetailsMap: Map<number, any> = new Map();

  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  setNewDetails(fighterDeatails: FighterDetails) {
    this.fightersDetailsMap.set(fighterDeatails._id, fighterDeatails);
  }

  async getFighterDetails(_id: number) {
    // implement this method
    // endpoint - `details/fighter/${_id}.json`;
    if (this.fightersDetailsMap.has(_id)) {
      return this.fightersDetailsMap.get(_id);
    }
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, "GET");
      let result = JSON.parse(atob(apiResult.content));
      this.fightersDetailsMap.set(_id, result)
      return result;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
