import { FighterDetails } from './FighterDetails';
import { fighterService } from './services/fightersService';
import { FighterHero } from './FighterHero';
import FightersView from './fightersView';

export class Ring {
    heroesSet = new Set();
    fighterOneImage:HTMLElement;
    fighterTwoImage:HTMLElement;
    fighterOneFullHealth:number;
    fighterTwoFullHealth:number;
    
constructor(private rootElement:HTMLElement){
    let checkboxes = rootElement.getElementsByClassName("hero");
    [].forEach.call(checkboxes, (el:HTMLInputElement) => el.addEventListener("change", (event) => {
        let e =event.target as HTMLInputElement
        if (e.checked) {
            this.heroesSet.add(e.id);
            if(this.heroesSet.size == 2){
                this.timeToFight();
            }
          } else {
            this.heroesSet.delete(e.id);
          }
    }))
        
    
}
    fight(selectedHero: FighterHero, rival: FighterHero) {
        let isHarm = true;
        let randInt = Math.floor(Math.random() * 2);
        let whoAttacks:FighterHero[] = !!(randInt)  ? [selectedHero, rival] : [rival, selectedHero];
        let attackHarm = whoAttacks[0].getHitPower() - whoAttacks[1].getBlockPower();
        attackHarm = attackHarm < 0 ? 0 : attackHarm;
        isHarm = attackHarm != 0;
        whoAttacks[1].health -= attackHarm;
        console.log(`${whoAttacks[0].name}:${whoAttacks[0].health} ---> ${whoAttacks[1].name}:${whoAttacks[1].health}` );
        if(this.fighterOneImage.id === whoAttacks[1]._id.toString() && isHarm){
            this.fighterOneImage.style.cssText = "border: 5px solid tomato";
            let width = Math.floor(whoAttacks[1].health * 100 / this.fighterOneFullHealth)+"%";
            (this.fighterOneImage.getElementsByClassName("life-info")[0] as HTMLElement).style.width = width;
            setTimeout(()=> this.fighterOneImage.style.border = "0px",400);
        }else if(this.fighterTwoImage.id === whoAttacks[1]._id.toString() && isHarm){
            this.fighterTwoImage.style.cssText = "border: 5px solid tomato";
            let width = Math.floor(whoAttacks[1].health * 100 / this.fighterTwoFullHealth)+"%";
            (this.fighterTwoImage.getElementsByClassName("life-info")[0] as HTMLElement).style.width = width;
            setTimeout(()=> this.fighterTwoImage.style.border = "0px",400);
        }
         
        if (whoAttacks[1].health < 0) {
            console.log(`Winner ${whoAttacks[0].name}`);
            this.rootElement.innerHTML = `<h1>${whoAttacks[0].name} win. Game over<h1>`;
            return;
        }
        whoAttacks = [whoAttacks[1], whoAttacks[0]];
        setTimeout(() => this.fight(whoAttacks[0],whoAttacks[1]), 1000);
    }

    async timeToFight(){
        var heroesIds = Array.from(this.heroesSet).map((e:string) => parseInt(e));
        let fighterOne:FighterDetails =  await fighterService.getFighterDetails(heroesIds[0]);
        let fighterTwo:FighterDetails =  await fighterService.getFighterDetails(heroesIds[1]);
        this.rootElement.innerHTML = `
        <div class="fighters">
            <div class="fighter" id="${fighterOne._id}">
                <h1>${fighterOne.name}</h1>
                <img src="${fighterOne.source}">
                <div class="life-info" style="background-color: green; width:100%;height:30px;"></div>
            </div>
            <h2>VS<h2> 
            <div class="fighter" id="${fighterTwo._id}">
                <h1>${fighterTwo.name}</h1>
                <img src="${fighterTwo.source}">
                <div class="life-info" style="background-color: green; width:100%;height:30px;"></div>
            </div>
        </div>`;
       let fighterHeroOne = new FighterHero(fighterOne);
       let fighterHeroTwo = new FighterHero(fighterTwo);
       this.fighterOneImage = this.rootElement.querySelector(`#\\3${fighterOne._id} `);
       this.fighterTwoImage = this.rootElement.querySelector(`#\\3${fighterTwo._id} `);
        this.fighterOneFullHealth = fighterOne.health;
        this.fighterTwoFullHealth = fighterTwo.health;
        this.fight(fighterHeroOne,fighterHeroTwo);
    }

}