import { FighterDetails } from './FighterDetails';
import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import { Ring } from './Ring';


class App {

  fightersDetailsSaved:Map<number,FighterDetails>;
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root') as HTMLElement;
  static loadingElement = document.getElementById('loading-overlay') as HTMLElement;

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
     
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
    
    
    
   new Ring(App.rootElement);
  }
}

export default App;

