import { Fighter } from './Fighter';
import View from './view';

class FighterView extends View {
  constructor(fighter:Fighter, handleClick:(ev:Event,f:Fighter)=>void) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter:Fighter, handleClick:(ev:Event,f:Fighter)=>void) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    this.element = this.createElement({tagName : 'div', className: 'fighter'});
    let tempElement = this.createElement({ tagName: 'div', className: 'fighter-element' });
    tempElement.append(imageElement, nameElement);
    tempElement.addEventListener('click', event => handleClick(event, fighter), false);
    let checkBox = this.createElement({tagName: 'input', className:'hero',attributes:{id:fighter._id,name:'heroName',type:'checkbox',value:fighter.name}});
    this.element.append(tempElement,checkBox);
  }

  createName(name:string) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source:string) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;